require stream,2.8.10
require caenelsmagnetps,1.1.0

# FAST-PS unit
iocshLoad("$(caenelsmagnetps_DIR)/caenelsfastps.iocsh", "Ch_name=FASTPS_1, IP_ADDR=192.168.0.1, P=LEBT:, R=PwrC-PSCH-01:, MAXVOL=20, MAXCUR=20, HICUR=17, HIHICUR=19")

# NGPS unit
iocshLoad("$(caenelsmagnetps_DIR)/caenelsngps.iocsh", "Ch_name=NGPS_1, IP_ADDR=192.168.0.2, P=MEBT:, R=PwrC-Quad-001:, MAXVOL=20, MAXCUR=300, HICUR=290, HIHICUR=295")

iocInit()
